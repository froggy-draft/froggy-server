module.exports = {
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  extends: ["eslint:recommended"],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["@typescript-eslint"],
  rules: {
    "max-len": [
      "error",
      {
        code: 120,
      },
    ],
    indent: ["error", 2],
    // Don't check unused vars with eslint. Typescript does a better job of this.
    "no-unused-vars": "off",
  },
};
