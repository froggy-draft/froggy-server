require("dotenv").config();

import https from "https";
import fs from "fs";

import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import { createProxyMiddleware } from "http-proxy-middleware";
import ms from "ms";

import { ensureFetchAllPlayersFromSleeper } from "./scrapers/sleeper/all";
import { fetchDraftedPlayersFromSleeper } from "./scrapers/sleeper/drafted";
import { authenticateWithUdk, verifyUdkAuthToken } from "./scrapers/udk/auth";
import { ensureFetchAllPlayersFromUdk } from "./scrapers/udk/all";
import { ERROR } from "./types";
import { sendError, scrambleUdkPlayers } from "./helpers";

const app = express();
const httpPort = process.env.HTTP_PORT || 3000;
const httpsPort = process.env.HTTPS_PORT || 3100;

const enableHttps = !!(
  process.env.ENABLE_HTTPS && process.env.ENABLE_HTTPS.toLowerCase() === "true"
);
const httpsCertPath = process.env.HTTPS_CERT_PATH || "";
const httpsKeyPath = process.env.HTTPS_KEY_PATH || "";

app.use(bodyParser.json());
app.use(cookieParser());

app.get("/api/sleeper/players/all", async (_req, res) => {
  try {
    const players = await ensureFetchAllPlayersFromSleeper();
    res.send(players);
  } catch (error) {
    console.error(error);
    sendError(res, 500, {
      error: ERROR.UNKNOWN,
      message: `Unable to fetch all players from Sleeper`,
    });
  }
});

app.get("/api/sleeper/players/drafted/:draftId", async (req, res) => {
  const { draftId } = req.params;
  try {
    const players = await fetchDraftedPlayersFromSleeper(draftId);
    res.send(players);
  } catch (error) {
    if (error.message === ERROR.INVALID_DRAFT_ID) {
      return sendError(res, 400, {
        error: ERROR.INVALID_DRAFT_ID,
        message: `Unable to fetch drafted players from invalid Sleeper Draft '${draftId}'`,
      });
    }
    console.error(error);
    sendError(res, 500, {
      error: ERROR.UNKNOWN,
      message: `Unable to fetch drafted players from Sleeper Draft '${draftId}'`,
    });
  }
});

app.post("/api/udk/auth", async (req, res) => {
  let udkUsername: string;
  let udkPassword: string;

  try {
    udkUsername = req.body.udkUsername;
    udkPassword = req.body.udkPassword;
    if (
      (!udkUsername && udkUsername !== "") ||
      (!udkPassword && udkPassword !== "")
    ) {
      throw new Error();
    }
  } catch (error) {
    return sendError(res, 400, {
      error: ERROR.MALFORMED_BODY,
      message: `The body of the UDK authentication request is malformed.`,
    });
  }

  try {
    const udkAuthToken = await authenticateWithUdk(udkUsername, udkPassword);
    res.cookie("udkAuthToken", udkAuthToken, {
      maxAge: ms("2w"),
      httpOnly: true,
      secure: enableHttps,
      sameSite: true,
    });
    res.sendStatus(200);
  } catch (error) {
    if (error.message === ERROR.INVALID_USERNAME_OR_PASSWORD) {
      return sendError(res, 401, {
        error: ERROR.INVALID_USERNAME_OR_PASSWORD,
        message: `UDK username or password is incorrect.`,
      });
    }
    console.error(error);
    sendError(res, 500, {
      error: ERROR.UNKNOWN,
      message: `Unable to authenticate with UDK.`,
    });
  }
});

app.get("/api/udk/auth/clear", async (_req, res) => {
  try {
    res.cookie("udkAuthToken", "", {
      maxAge: 0,
      httpOnly: true,
      secure: enableHttps,
      sameSite: true,
    });
    res.sendStatus(200);
  } catch (error) {
    console.error(error);
    sendError(res, 500, {
      error: ERROR.UNKNOWN,
      message: `Unable to clear UDK authentication.`,
    });
  }
});

app.get("/api/udk/auth/verify", async (req, res) => {
  let udkAuthToken: string;

  try {
    udkAuthToken = req.cookies.udkAuthToken;
    if (!udkAuthToken) {
      throw new Error();
    }
    await verifyUdkAuthToken(udkAuthToken);
  } catch (error) {
    return sendError(res, 401, {
      error: ERROR.INVALID_UDK_AUTH_TOKEN,
      message: `Could not verify UDK authentication.`,
    });
  }

  res.sendStatus(200);
});

app.get("/api/udk/players/all", async (req, res) => {
  let udkAuthToken: string;

  try {
    udkAuthToken = req.cookies.udkAuthToken;
    if (!udkAuthToken) {
      throw new Error();
    }
    await verifyUdkAuthToken(udkAuthToken);
  } catch (error) {
    return sendError(res, 401, {
      error: ERROR.INVALID_UDK_AUTH_TOKEN,
      message: `Could not verify UDK authentication.`,
    });
  }

  try {
    const players = await ensureFetchAllPlayersFromUdk();
    const shouldScramble =
      req.query.scramble &&
      typeof req.query.scramble === "string" &&
      req.query.scramble.toLowerCase() === "true";
    if (shouldScramble) {
      return res.send(scrambleUdkPlayers(players));
    }
    res.send(players);
  } catch (error) {
    console.error(error);
    sendError(res, 500, {
      error: ERROR.UNKNOWN,
      message: `Unable to fetch all players from UDK`,
    });
  }
});

// Finally, we will proxy all remaining requests to the UI server...
app.use(
  createProxyMiddleware({
    target: "http://localhost:5000",
    changeOrigin: true,
  })
);

if (enableHttps) {
  const httpsServer = https.createServer(
    {
      key: fs.readFileSync(httpsKeyPath),
      cert: fs.readFileSync(httpsCertPath),
    },
    app
  );
  const httpServer = express();

  httpsServer.listen(httpsPort, () =>
    console.log(`HTTPS server listening on port ${httpsPort}!`)
  );

  // redirect all http traffic to https
  httpServer.get("*", (req, res) => {
    res.redirect("https://" + req.headers.host + req.url);
  });
  httpServer.listen(httpPort, () =>
    console.log(`HTTP server listening on port ${httpPort}!`)
  );
} else {
  const httpServer = app;
  console.warn("WARNING: HTTPS is not enabled! Traffic will not be encrypted.");
  httpServer.listen(httpPort, () =>
    console.log(`HTTP server listening on port ${httpPort}!`)
  );
}
