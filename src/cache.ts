import ms from "ms";
import { RankedPlayersByPosition } from "./types";

type CacheEntry<T> = {
  value: T;
  expiresAt: number;
} | null;

type Cache = {
  allSleeperPlayers: CacheEntry<RankedPlayersByPosition>;
  allUdkPlayers: CacheEntry<RankedPlayersByPosition>;
};

let cache: Cache = {
  allSleeperPlayers: null,
  allUdkPlayers: null,
};

const clearExpired = () => {
  Object.keys(cache).forEach((key) => {
    const cacheKey = key as keyof Cache;
    const cacheItem = cache[cacheKey];
    if (cacheItem && cacheItem.expiresAt <= Date.now()) {
      cache[cacheKey] = null;
    }
  });
};

export const getAllSleeperPlayers = () => {
  clearExpired();
  if (cache.allSleeperPlayers) {
    return cache.allSleeperPlayers.value;
  }
  return null;
};

export const saveAllSleeperPlayers = (players: RankedPlayersByPosition) => {
  const expiresAt = Date.now() + ms("1w");
  cache.allSleeperPlayers = { value: players, expiresAt };
};

export const getAllUdkPlayers = () => {
  clearExpired();
  if (cache.allUdkPlayers) {
    return cache.allUdkPlayers.value;
  }
  return null;
};

export const saveAllUdkPlayers = (players: RankedPlayersByPosition) => {
  const expiresAt = Date.now() + ms("1w");
  cache.allUdkPlayers = { value: players, expiresAt };
};
