import * as cache from "../../../cache";
import { RankedPlayersByPosition, Position } from "../../../types";
import { makeWarmCacheFn } from "../../../helpers";
import { openBrowser, closeBrowser, openPage } from "../../shared/helpers";
import { ensureFetchAllPlayersFromSleeper } from "../../sleeper/all";
import { ensureLoggedIn } from "../auth/helpers";
import { fetchPlayersFromUdk, udkUrl } from "./helpers";

const { OWN_UDK_USERNAME, OWN_UDK_PASSWORD } = process.env;

if (!OWN_UDK_USERNAME) {
  throw new Error("OWN_UDK_USERNAME is required");
}
if (!OWN_UDK_PASSWORD) {
  throw new Error("OWN_UDK_PASSWORD is required");
}

export const fetchAllPlayersFromUdk = async (): Promise<
  RankedPlayersByPosition
> => {
  const browser = await openBrowser();
  const page = await openPage(browser);
  try {
    await page.goto(udkUrl);
    const [allSleeperPlayers] = await Promise.all([
      ensureFetchAllPlayersFromSleeper(),
      ensureLoggedIn(page, OWN_UDK_USERNAME, OWN_UDK_PASSWORD),
    ]);

    if (!allSleeperPlayers) {
      throw new Error("Could not get all players");
    }

    const result = {
      [Position.QB]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.QB
      ),
      [Position.RB]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.RB
      ),
      [Position.WR]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.WR
      ),
      [Position.TE]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.TE
      ),
      [Position.K]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.K
      ),
      [Position.DEF]: await fetchPlayersFromUdk(
        page,
        allSleeperPlayers,
        Position.DEF
      ),
    };
    closeBrowser(browser);
    return result;
  } catch (error) {
    closeBrowser(browser);
    throw error;
  }
};

export const ensureFetchAllPlayersFromUdk = async ({
  force = false,
}: {
  force?: boolean;
} = {}): Promise<RankedPlayersByPosition> => {
  if (!force) {
    const cachedPlayers = cache.getAllUdkPlayers();
    if (cachedPlayers) {
      return cachedPlayers;
    }
  }
  const players = await fetchAllPlayersFromUdk();
  cache.saveAllUdkPlayers(players);
  return players;
};

const warmCache = makeWarmCacheFn({
  fn: () => ensureFetchAllPlayersFromUdk({ force: true }),
  cacheName: "All UDK Players",
});

warmCache();
