export const validatePlayers = (players: { name: string }[]) => {
  players.forEach((player) => {
    if (
      !player.name.includes(" ") ||
      player.name.includes("(") ||
      player.name.includes(")")
    ) {
      throw new Error(`Player "${player.name}" likely has an incorrect name`);
    }

    // TODO: more validation here. rank, team, etc.
  });
};
