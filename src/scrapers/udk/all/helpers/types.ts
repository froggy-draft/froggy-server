import { Player, RankedPlayer } from "../../../../types";

export type UDKScrapeResultPlayer = Player &
  Pick<
    RankedPlayer,
    "rank" | "upside" | "risk" | "tier" | "andyRank" | "mikeRank" | "jasonRank"
  >;

export type UDKScrapeResultOverallPlayer = Player &
  Pick<RankedPlayer, "overallRank" | "andyRank" | "mikeRank" | "jasonRank">;
