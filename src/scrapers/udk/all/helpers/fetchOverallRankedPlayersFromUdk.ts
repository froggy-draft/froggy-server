import { Page } from "puppeteer";

import {
  Position,
  OverallRankedPlayer,
  RankedPlayersByPosition,
} from "../../../../types";
import { reconcilePlayerIds } from "../../../sleeper/helpers";
import { udkOverallRankingsUrl } from "./constants";
import { validatePlayers } from "./validatePlayers";
import { UDKScrapeResultOverallPlayer } from "./types";
import * as browserScripts from "./browserScripts";

export const fetchOverallRankedPlayersFromUdk = async (
  page: Page,
  allSleeperPlayers: RankedPlayersByPosition,
  position: Position
): Promise<UDKScrapeResultOverallPlayer[]> => {
  console.log(`Fetching ${position} players from UDK...`);

  await page.goto(udkOverallRankingsUrl);

  await page.waitForSelector("#udk-top-200-table tbody tr");
  const scrapeResultPlayers = await browserScripts.evaluateForOverallRank(
    page,
    position
  );

  reconcilePlayerIds(allSleeperPlayers[position], scrapeResultPlayers);

  const players = scrapeResultPlayers.map(
    (scrapeResultPlayer): OverallRankedPlayer => {
      const matchingPlayer = allSleeperPlayers[position].find(
        (x) => x.id === scrapeResultPlayer.id
      );
      if (!matchingPlayer) {
        throw new Error(`Unable to match player "${scrapeResultPlayer.name}"`);
      }
      return {
        id: matchingPlayer.id,
        name: matchingPlayer.name,
        team: matchingPlayer.team,
        position: matchingPlayer.position,
        overallRank: scrapeResultPlayer.overallRank,
        andyRank: scrapeResultPlayer.andyRank,
        mikeRank: scrapeResultPlayer.mikeRank,
        jasonRank: scrapeResultPlayer.jasonRank,
      };
    }
  );

  validatePlayers(players);

  console.log(`Fetched ${position} players from UDK.`);

  return players;
};
