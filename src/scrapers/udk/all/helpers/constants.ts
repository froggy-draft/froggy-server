import { Position } from "../../../../types";

export const udkUrl =
  "https://thefantasyfootballers.com/2023-ultimate-draft-kit";
export const udkRankingsUrl = `${udkUrl}/udk-position-rankings`;
export const udkRankingsUrlsByPosition: Record<Position, string> = {
  [Position.QB]: `${udkRankingsUrl}?position=QB`,
  [Position.RB]: `${udkRankingsUrl}?position=RB`,
  [Position.WR]: `${udkRankingsUrl}?position=WR`,
  [Position.TE]: `${udkRankingsUrl}?position=TE`,
  [Position.K]: `${udkRankingsUrl}?position=K`,
  [Position.DEF]: `${udkRankingsUrl}?position=D`,
};
export const udkOverallRankingsUrl = `${udkUrl}/udk-top-200-list/?scoring=ppr`;
