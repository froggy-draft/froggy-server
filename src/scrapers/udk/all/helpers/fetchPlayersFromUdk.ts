import { Page } from "puppeteer";

import {
  Position,
  RankedPlayer,
  RankedPlayersByPosition,
} from "../../../../types";
import { reconcilePlayerIds } from "../../../sleeper/helpers";
import { udkRankingsUrlsByPosition } from "./constants";
import { UDKScrapeResultOverallPlayer } from "./types";
import { validatePlayers } from "./validatePlayers";
import { fetchOverallRankedPlayersFromUdk } from "./fetchOverallRankedPlayersFromUdk";
import * as browserScripts from "./browserScripts";

export const fetchPlayersFromUdk = async (
  page: Page,
  allSleeperPlayers: RankedPlayersByPosition,
  position: Position
): Promise<RankedPlayer[]> => {
  const overallRankedPlayersAtPosition = await fetchOverallRankedPlayersFromUdk(
    page,
    allSleeperPlayers,
    position
  );

  const url = udkRankingsUrlsByPosition[position];
  await page.goto(url);

  const hasTiers = position !== Position.K && position !== Position.DEF;

  if (hasTiers) {
    await page.waitForSelector(".ffb-league-scoring");
    await page.select(".ffb-league-scoring", "PPR (4pt QB)");
  }

  await page.waitForSelector("#udk-rankings-table tbody tr");
  const scrapeResultPlayers = await browserScripts.evaluate(page, position);

  reconcilePlayerIds(allSleeperPlayers[position], scrapeResultPlayers);

  const players = scrapeResultPlayers.map(
    (scrapeResultPlayer): RankedPlayer => {
      const matchingPlayer = allSleeperPlayers[position].find(
        (x) => x.id === scrapeResultPlayer.id
      );
      if (!matchingPlayer) {
        throw new Error(`Unable to match player "${scrapeResultPlayer.name}"`);
      }
      const overallRankedMatchingPlayer: UDKScrapeResultOverallPlayer = overallRankedPlayersAtPosition.find(
        (x) => x.id === scrapeResultPlayer.id
      ) || {
        ...scrapeResultPlayer,
        overallRank: null,
      };
      return {
        id: matchingPlayer.id,
        name: matchingPlayer.name,
        team: matchingPlayer.team,
        position: matchingPlayer.position,
        rank: scrapeResultPlayer.rank,
        customRank: scrapeResultPlayer.rank,
        overallRank: overallRankedMatchingPlayer.overallRank,
        andyRank:
          scrapeResultPlayer.andyRank || overallRankedMatchingPlayer.andyRank,
        mikeRank:
          scrapeResultPlayer.mikeRank || overallRankedMatchingPlayer.mikeRank,
        jasonRank:
          scrapeResultPlayer.jasonRank || overallRankedMatchingPlayer.jasonRank,
        tier: scrapeResultPlayer.tier,
        bye: matchingPlayer.bye,
        pointsStd: matchingPlayer.pointsStd,
        pointsPpr: matchingPlayer.pointsPpr,
        pointsHalfPpr: matchingPlayer.pointsHalfPpr,
        upside: scrapeResultPlayer.upside,
        risk: scrapeResultPlayer.risk,
        adpStd: matchingPlayer.adpStd,
        adpPpr: matchingPlayer.adpPpr,
        adpHalfPpr: matchingPlayer.adpHalfPpr,
        injury: matchingPlayer.injury,
        injuryMessage: matchingPlayer.injuryMessage,
        auctionValue: matchingPlayer.auctionValue,
      };
    }
  );

  validatePlayers(players);

  return players;
};
