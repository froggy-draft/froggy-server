import { Page } from "puppeteer";

import { Position } from "../../../../types";
import { UDKScrapeResultPlayer, UDKScrapeResultOverallPlayer } from "./types";

export const evaluate = async (page: Page, position: Position) =>
  await page.evaluate(
    ({
      position,
      isK,
      isDef,
    }: {
      position: Position;
      isK: boolean;
      isDef: boolean;
    }) => {
      /*****************************/
      /* Logic to extract a kicker */
      /*****************************/

      const extractKicker = (cell: Element): UDKScrapeResultPlayer => {
        const playerCell = cell.childNodes[0].childNodes[0]
          .lastChild as ChildNode;
        const rankCell = cell.childNodes[1];
        const andyCell = cell.childNodes[2];
        const jasonCell = cell.childNodes[3];
        const mikeCell = cell.childNodes[4];

        const name = (playerCell.childNodes[1].textContent || "").trim();

        const team = (playerCell.childNodes[3].textContent || "") // 'HOU (10)'
          .split("(")[0] // 'HOU '
          .trim(); // 'HOU'

        const rank = Number((rankCell.textContent || "").trim());

        const andyRank = Number((andyCell.textContent || "").trim());
        const mikeRank = Number((mikeCell.textContent || "").trim());
        const jasonRank = Number((jasonCell.textContent || "").trim());

        return {
          id: "",
          name,
          team,
          position,
          rank,
          risk: null,
          upside: null,
          tier: null,
          andyRank,
          mikeRank,
          jasonRank,
        };
      };

      /******************************/
      /* Logic to extract a defense */
      /******************************/

      const extractDefense = (cell: Element): UDKScrapeResultPlayer => {
        const playerCell = cell.childNodes[0].childNodes[0]
          .lastChild as ChildNode;
        const rankCell = cell.childNodes[1];
        const andyCell = cell.childNodes[2];
        const jasonCell = cell.childNodes[3];
        const mikeCell = cell.childNodes[4];

        const name = (playerCell.childNodes[1].textContent || "").trim();

        const rank = Number((rankCell.textContent || "").trim());

        const andyRank = Number((andyCell.textContent || "").trim());
        const mikeRank = Number((mikeCell.textContent || "").trim());
        const jasonRank = Number((jasonCell.textContent || "").trim());

        return {
          id: "",
          name,
          team: "",
          position,
          rank,
          risk: null,
          upside: null,
          tier: null,
          andyRank,
          mikeRank,
          jasonRank,
        };
      };

      /************************************/
      /* Logic to extract a tiered player */
      /************************************/

      const extractTieredPlayer = (
        cell: Element
      ): UDKScrapeResultPlayer | null => {
        const classNames = cell.className.split(" ");
        const isTierHeader = classNames.includes("group");
        if (isTierHeader) {
          // skip tier header cells
          return null;
        }

        const playerCell = cell.childNodes[0].childNodes[0]
          .lastChild as ChildNode;
        const rankCell = cell.childNodes[1];
        const riskCell = cell.childNodes[3];
        const upsideCell = cell.childNodes[4];
        const tierCell = cell.childNodes[6];

        const name = (playerCell.childNodes[1].textContent || "").trim();

        const team = (playerCell.childNodes[3].textContent || "") // 'HOU (10)'
          .split("(")[0] // 'HOU '
          .trim(); // 'HOU'

        const rank = Number((rankCell.textContent || "").trim());
        const risk = Number(
          (riskCell.childNodes[0].childNodes[3].textContent || "").trim()
        );
        const upside = Number(
          (upsideCell.childNodes[0].childNodes[3].textContent || "").trim()
        );
        const tier = Number((tierCell.textContent || "").trim());

        return {
          id: "",
          name,
          team,
          position,
          rank,
          risk,
          upside,
          tier,
          andyRank: null,
          mikeRank: null,
          jasonRank: null,
        };
      };

      /**************/
      /* Main Logic */
      /**************/

      let players: UDKScrapeResultPlayer[] = [];

      const cells = document.querySelectorAll("#udk-rankings-table tbody tr");
      cells.forEach((cell) => {
        let player: UDKScrapeResultPlayer | null;

        if (isDef) {
          player = extractDefense(cell);
        } else if (isK) {
          player = extractKicker(cell);
        } else {
          let maybePlayer = extractTieredPlayer(cell);
          if (!maybePlayer) {
            return;
          }
          player = maybePlayer;
        }

        players.push(player);
      });

      return players;
    },
    {
      position,
      isK: position === Position.K,
      isDef: position === Position.DEF,
    }
  );

export const evaluateForOverallRank = async (page: Page, position: Position) =>
  await page.evaluate(
    ({ position }: { position: Position }) => {
      const extractOverallRankedPlayer = (
        cell: Element
      ): UDKScrapeResultOverallPlayer | null => {
        const playerCell = cell.childNodes[0].childNodes[0]
          .lastChild as ChildNode;
        const posCell = cell.childNodes[1];
        const rankCell = cell.childNodes[2];
        const andyCell = cell.childNodes[3];
        const jasonCell = cell.childNodes[4];
        const mikeCell = cell.childNodes[5];

        const name = (playerCell.childNodes[1].textContent || "").trim();

        const team = (playerCell.childNodes[3].textContent || "") // 'HOU (10)'
          .split("(")[0] // 'HOU '
          .trim(); // 'HOU'

        const actualPosition = (posCell.textContent || "").trim();
        const overallRank = Number((rankCell.textContent || "").trim());

        const andyRank = Number((andyCell.textContent || "").trim());
        const mikeRank = Number((mikeCell.textContent || "").trim());
        const jasonRank = Number((jasonCell.textContent || "").trim());

        if (actualPosition !== position) {
          return null;
        }

        return {
          id: "",
          name,
          team,
          position,
          overallRank,
          andyRank,
          mikeRank,
          jasonRank,
        };
      };

      let players: UDKScrapeResultOverallPlayer[] = [];

      const cells = document.querySelectorAll("#udk-top-200-table tbody tr");
      cells.forEach((cell) => {
        const player = extractOverallRankedPlayer(cell);
        if (player) {
          players.push(player);
        }
      });

      return players;
    },
    {
      position,
    }
  );
