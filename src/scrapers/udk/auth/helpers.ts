import { Page } from "puppeteer";
import { ERROR } from "../../../types";

export const ensureLoggedIn = async (
  page: Page,
  udkUsername: string,
  udkPassword: string
) => {
  const getIsLoggedIn = async () => {
    // TODO: Does loggedInPromise1 ever actually hit anymore?
    const loggedInPromise1 = page
      .waitForSelector(".UDKRankingsTable tbody tr")
      .then(() => true);
    const loggedInPromise2 = page
      .waitForSelector(".UDKUpdatesTable")
      .then(() => true);
    const loggedInPromise3 = page
      .waitForSelector(".ffb-account-product.active")
      .then(() => true);
    const notLoggedInPromise = page
      .waitForSelector("#user_login")
      .then(() => false);
    return await Promise.race([
      loggedInPromise1,
      loggedInPromise2,
      loggedInPromise3,
      notLoggedInPromise,
    ]);
  };

  if (!(await getIsLoggedIn())) {
    await page.type("#user_login", udkUsername);
    await page.type("#user_pass", udkPassword);
    await page.click("#wp-submit");

    if (!(await getIsLoggedIn())) {
      throw new Error(ERROR.INVALID_USERNAME_OR_PASSWORD);
    }
  }
};
