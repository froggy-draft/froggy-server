import jwt from "jsonwebtoken";
import { ERROR } from "../../../types";
import { openBrowser, openPage, closeBrowser } from "../../shared/helpers";
import { ensureLoggedIn } from "./helpers";

const udkJwtSecret = process.env.UDK_JWT_SECRET;
const udkUrl = "https://thefantasyfootballers.com/2023-ultimate-draft-kit";

if (!udkJwtSecret) {
  throw new Error("UDK_JWT_SECRET must be defined");
}

export const authenticateWithUdk = async (
  udkUsername: string,
  udkPassword: string
) => {
  const browser = await openBrowser();
  const page = await openPage(browser);
  try {
    await page.goto(udkUrl);
    await ensureLoggedIn(page, udkUsername, udkPassword);
    closeBrowser(browser);
    // TODO: async
    return jwt.sign({ authenticated: true, udkUsername }, udkJwtSecret);
  } catch (error) {
    closeBrowser(browser);
    throw error;
  }
};

export const verifyUdkAuthToken = async (udkAuthToken: string) => {
  try {
    // TODO: async
    return jwt.verify(udkAuthToken, udkJwtSecret);
  } catch (error) {
    throw new Error(ERROR.INVALID_UDK_AUTH_TOKEN);
  }
};
