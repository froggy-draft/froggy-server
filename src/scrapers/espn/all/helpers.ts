import axios from "axios";
import _ from "lodash";
import { Position } from "../../../types";
import { EspnResponse, EspnPlayer } from "./types";

const positionMap: Record<number, Position> = {
  1: Position.QB,
  2: Position.RB,
  3: Position.WR,
  4: Position.TE,
  5: Position.K,
  16: Position.DEF,
};

const teamMap: Record<number, string> = {
  34: "HOU",
  33: "BAL",
  30: "JAX",
  29: "CAR",
  28: "WAS",
  27: "TB",
  26: "SEA",
  25: "SF",
  24: "LAC",
  23: "PIT",
  22: "ARI",
  21: "PHI",
  20: "NYJ",
  19: "NYG",
  18: "NO",
  17: "NE",
  16: "MIN",
  15: "MIA",
  14: "LAR",
  13: "LV",
  12: "KC",
  11: "IND",
  10: "TEN",
  9: "GB",
  8: "DET",
  7: "DEN",
  6: "DAL",
  5: "CLE",
  4: "CIN",
  3: "CHI",
  2: "BUF",
  1: "ATL",
};

const getPositionFromId = (id: number) => {
  const position = positionMap[id];
  if (!position) {
    return null;
  }
  return position;
};

const getTeamFromId = (id: number) => {
  const team = teamMap[id];
  if (!team) {
    return null;
  }
  return team;
};

// TODO: Update this for 2022 or delete this.
export const fetchEspnPlayers = async (): Promise<EspnPlayer[]> => {
  const url =
    "https://fantasy.espn.com/apis/v3/games/ffl/seasons/2020/segments/0/leagues/20847275";
  const params = ["view=kona_player_info_edit_draft_strategy"].join("&");
  const res = await axios.get<EspnResponse>(`${url}?${params}`, {
    headers: {
      "X-Fantasy-Filter": JSON.stringify({
        players: {
          filterStatsForSplitTypeIds: { value: [0] },
          filterStatsForSourceIds: { value: [1] },
          filterStatsForExternalIds: { value: [2020] },
          sortDraftRanks: { sortPriority: 2, sortAsc: true, value: "STANDARD" },
          sortPercOwned: { sortPriority: 3, sortAsc: false },
          filterStatsForTopScoringPeriodIds: {
            value: 2,
            additionalValue: ["002020", "102020", "002019", "022020"],
          },
        },
      }),
    },
  });

  const players = res.data.players
    .map((x) => ({
      id: "",
      name: `${x.player.firstName} ${x.player.lastName}`,
      position: getPositionFromId(x.player.defaultPositionId),
      team: getTeamFromId(x.player.proTeamId),
      auctionValue: x.draftAuctionValue,
    }))
    .filter((x) => x.position && x.team) as EspnPlayer[];

  return players;
};
