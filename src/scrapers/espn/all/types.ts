import { Player } from "../../../types";

export * from "../../../types";

export type EspnPlayerRemote = {
  draftAuctionValue: number;
  player: {
    defaultPositionId: number;
    proTeamId: number;
    firstName: string;
    lastName: string;
  };
};

export type EspnResponse = {
  players: EspnPlayerRemote[];
};

export type EspnPlayer = {
  auctionValue: number;
} & Player;
