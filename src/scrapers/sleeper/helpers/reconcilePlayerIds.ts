import memoize from "memoizee";
import { Player, RankedPlayer } from "../../../types";

type NameFormat = "normal" | "firstInitial";

const normalizeName = memoize((name: string, format: NameFormat) => {
  const normalizedName = name
    .replace(/\./g, "")
    .replace(/'/g, "")
    .replace(/-/g, "")
    .replace(/,/g, "")
    .replace("D/ST", "")
    .replace(/ I$/g, "")
    .replace(/ II$/g, "")
    .replace(/ III$/g, "")
    .replace(/ IV$/g, "")
    .replace(/ V$/g, "")
    .replace(/ VI$/g, "")
    .replace(/ Jr$/g, "")
    .replace(/ Sr$/g, "")
    // Hack for William Fuller vs Will Fuller
    .replace(/^William /, "Will ")
    // Hack for Jeffery Wilson vs Jeff Wilson
    .replace(/^Jeffery /, "Jeff ")
    // Hack for Christopher Herndon vs Chris Herndon
    .replace(/^Christopher /, "Chris ")
    // Hack for Eddy Piñeiro vs Eddy Pineiro
    .replace(/ Piñeiro$/, " Pineiro")
    .trim();

  if (format === "firstInitial") {
    const nameParts = normalizedName.split(" ");
    return `${nameParts[0].slice(0, 1)}. ${nameParts[nameParts.length - 1]}`;
  }

  return normalizedName;
});

type OptionsOfReconcilePlayerIds = {
  nameFormat?: NameFormat;
};

export const reconcilePlayerIds = (
  playersWithIds: RankedPlayer[],
  playersWithoutIds: Player[],
  options: OptionsOfReconcilePlayerIds = {}
) => {
  playersWithoutIds.forEach((playerWithoutId) => {
    const matchingPlayer = playersWithIds.find((playerWithId) => {
      const playerWithIdName = normalizeName(
        playerWithId.name,
        options.nameFormat ?? "normal"
      );
      const playerWithoutIdName = normalizeName(
        playerWithoutId.name,
        options.nameFormat ?? "normal"
      );
      return (
        (playerWithIdName.includes(playerWithoutIdName) ||
          playerWithoutIdName.includes(playerWithIdName)) &&
        (playerWithId.team === playerWithoutId.team ||
          playerWithId.position === playerWithId.position)
      );
    });
    if (!matchingPlayer) {
      console.warn(
        `Could not reconcile player id for player "${playerWithoutId.name}"`
      );
      return;
    }
    playerWithoutId.id = matchingPlayer.id;
  });
};
