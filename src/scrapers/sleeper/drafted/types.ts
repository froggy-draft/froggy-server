import { Position } from "../../../types";

export type ResponseDraftPick = {
  player_id: string; // "4034"
  picked_by: string; // "0"
  pick_no: number; // 1
  metadata: {
    years_exp: string; // "4"
    team: string; // "CAR"
    status: string; // "Active"
    sport: string; // "nfl"
    position: Position; // "RB"
    player_id: string; // "4034"
    number: string; // "22"
    news_updated: string; // "1628904326085"
    last_name: string; // "McCaffrey"
    injury_status: string; // ""
    first_name: string; // "Christian"
  };
};

export type ResponseData = {
  data: {
    draft_picks: ResponseDraftPick[];
  };
};
