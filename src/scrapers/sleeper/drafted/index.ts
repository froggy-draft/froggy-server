import { PlayersByPosition, Position } from "../../../types";
import { ensureFetchAllPlayersFromSleeper } from "../all";
import { fetchDraftedPlayers, extractDraftedPlayers } from "./helpers";

export const fetchDraftedPlayersFromSleeper = async (
  draftId: string
): Promise<PlayersByPosition> => {
  const [allPlayers, draftedPlayers] = await Promise.all([
    ensureFetchAllPlayersFromSleeper(),
    fetchDraftedPlayers(draftId),
  ]);

  if (!allPlayers) {
    throw new Error("Could not get all players");
  }
  if (!draftedPlayers) {
    throw new Error("Could not get drafted players");
  }

  const players: PlayersByPosition = {
    QB: extractDraftedPlayers(allPlayers, draftedPlayers, Position.QB),
    RB: extractDraftedPlayers(allPlayers, draftedPlayers, Position.RB),
    WR: extractDraftedPlayers(allPlayers, draftedPlayers, Position.WR),
    TE: extractDraftedPlayers(allPlayers, draftedPlayers, Position.TE),
    K: extractDraftedPlayers(allPlayers, draftedPlayers, Position.K),
    DEF: extractDraftedPlayers(allPlayers, draftedPlayers, Position.DEF),
  };

  return players;
};
