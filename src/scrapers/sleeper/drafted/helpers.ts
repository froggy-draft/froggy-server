import axios from "axios";
import _ from "lodash";
import { Player, Position, RankedPlayersByPosition } from "../../../types";
import { ResponseData } from "./types";
import { reconcilePlayerIds } from "../helpers";

export const extractDraftedPlayers = (
  allPlayersByPosition: RankedPlayersByPosition,
  allDraftedPlayers: Player[],
  position: Position
) => {
  const allPlayersOfPosition = allPlayersByPosition[position];
  const draftedPlayersOfPosition = allDraftedPlayers.filter(
    (x) => x.position === position
  );
  reconcilePlayerIds(allPlayersOfPosition, draftedPlayersOfPosition);
  return draftedPlayersOfPosition;
};

export const convertFromRemote = (data: ResponseData): Player[] => {
  return data.data.draft_picks.map((x) => ({
    id: x.player_id,
    name: `${x.metadata.first_name} ${x.metadata.last_name}`,
    team: x.metadata.team,
    position: x.metadata.position,
  }));
};

export const fetchDraftedPlayers = async (
  draftId: string
): Promise<Player[]> => {
  const url = "https://sleeper.app/graphql";
  const headers = {
    "Content-Type": "application/json",
  };
  const body = JSON.stringify({
    operationName: "get_draft",
    variables: {},
    query:
      // eslint-disable-next-line max-len
      `query get_draft {\n        get_draft(sport: "nfl",draft_id: "${draftId}"){\n          created\n          creators\n          draft_id\n          draft_order\n          last_message_time\n          last_message_id\n          last_picked\n          league_id\n          metadata\n          season\n          season_type\n          settings\n          sport\n          status\n          start_time\n          type\n        }\n\n        user_drafts_by_draft(draft_id: "${draftId}"){\n          user_id\n          user_display_name\n          user_avatar\n          user_is_bot\n        }\n\n        \n        draft_picks(draft_id: "${draftId}") {\n          player_id\n          pick_no\n          picked_by\n          metadata\n        }\n      \n      }`,
  });
  const res = await axios.post<ResponseData>(url, body, {
    headers,
  });
  const { data } = res;
  const rankedPlayers = convertFromRemote(data);
  return rankedPlayers;
};
