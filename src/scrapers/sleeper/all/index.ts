import * as cache from "../../../cache";
import { makeWarmCacheFn } from "../../../helpers";
import { RankedPlayersByPosition, Position } from "./types";
import { fetchPlayers, alignRanksToIndices } from "./helpers";

export const fetchAllPlayersFromSleeper = async (): Promise<
  RankedPlayersByPosition
> => {
  const players = await fetchPlayers();
  return {
    [Position.QB]: alignRanksToIndices(
      players.filter((x) => x.position === Position.QB)
    ),
    [Position.RB]: alignRanksToIndices(
      players.filter((x) => x.position === Position.RB)
    ),
    [Position.WR]: alignRanksToIndices(
      players.filter((x) => x.position === Position.WR)
    ),
    [Position.TE]: alignRanksToIndices(
      players.filter((x) => x.position === Position.TE)
    ),
    [Position.K]: alignRanksToIndices(
      players.filter((x) => x.position === Position.K)
    ),
    [Position.DEF]: alignRanksToIndices(
      players.filter((x) => x.position === Position.DEF)
    ),
  };
};

export const ensureFetchAllPlayersFromSleeper = async ({
  force = false,
}: {
  force?: boolean;
} = {}): Promise<RankedPlayersByPosition> => {
  if (!force) {
    const cachedPlayers = cache.getAllSleeperPlayers();
    if (cachedPlayers) {
      return cachedPlayers;
    }
  }
  const players = await fetchAllPlayersFromSleeper();
  cache.saveAllSleeperPlayers(players);
  return players;
};

const warmCache = makeWarmCacheFn({
  fn: () => ensureFetchAllPlayersFromSleeper({ force: true }),
  cacheName: "All Sleeper Players",
});

warmCache();
