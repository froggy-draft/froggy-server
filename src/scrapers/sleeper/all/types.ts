export * from "../../../types";

import { Position } from "../../../types";

export type PlayerRemote = {
  week: any;
  team: string;
  stats: {
    adp_std: number;
    adp_ppr: number;
    adp_half_ppr: number;
    pts_std: number;
    pts_ppr: number;
    pts_half_ppr: number;
  };
  sport: string;
  season_type: string;
  season: string;
  player_id: string;
  player: {
    years_exp: number;
    team: string;
    position: Position;
    news_updated: number;
    metadata: any;
    last_name: string;
    injury_status: any;
    injury_start_date: any;
    injury_notes: any;
    injury_body_part: any;
    first_name: string;
    fantasy_positions: Position[];
  };
  opponent: any;
  game_id: string;
  date: any;
  company: string;
  category: string;
};
