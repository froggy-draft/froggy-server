import axios from "axios";
import _ from "lodash";
import { RankedPlayer, PlayerRemote, Position } from "./types";
import { byeWeeksByTeam } from "../../../constants";
import { fetchEspnPlayers } from "../../espn/all";
import { reconcilePlayerIds } from "../helpers";

const addAuctionValuesToRankedPlayers = async (
  rankedPlayers: RankedPlayer[]
) => {
  const espnPlayers = await fetchEspnPlayers();
  reconcilePlayerIds(rankedPlayers, espnPlayers);
  const rankedPlayersById = _.keyBy(rankedPlayers, "id");
  espnPlayers.forEach((espnPlayer) => {
    const matchingPlayer = rankedPlayersById[espnPlayer.id];
    if (matchingPlayer) {
      matchingPlayer.auctionValue = espnPlayer.auctionValue;
    }
  });
};

export const getByeWeekForTeam = (team: string) => {
  return byeWeeksByTeam[team] || null;
};

export const alignRanksToIndices = (
  players: RankedPlayer[]
): RankedPlayer[] => {
  return players.map((player, index) => ({
    ...player,
    rank: index + 1,
    customRank: index + 1,
  }));
};

export const convertFromRemote = (
  playersRemote: PlayerRemote[]
): RankedPlayer[] => {
  return playersRemote.map((playerRemote, index) => {
    const playerData = playerRemote.player;
    const statsData = playerRemote.stats;
    const team =
      playerData.position === Position.DEF
        ? playerRemote.player_id
        : playerData.team;
    return {
      id: playerRemote.player_id,
      name: `${playerData.first_name} ${playerData.last_name}`,
      position: playerData.position,
      rank: index + 1,
      customRank: index + 1,
      overallRank: index + 1,
      andyRank: null,
      mikeRank: null,
      jasonRank: null,
      tier: null,
      team,
      bye: getByeWeekForTeam(team),
      pointsStd: statsData.pts_std,
      pointsPpr: statsData.pts_ppr,
      pointsHalfPpr: statsData.pts_half_ppr,
      risk: null,
      upside: null,
      adpStd: statsData.adp_std,
      adpPpr: statsData.adp_ppr,
      adpHalfPpr: statsData.adp_half_ppr,
      injury: playerData.injury_status,
      injuryMessage: playerData.injury_notes,
      auctionValue: null,
    };
  });
};

export const fetchPlayers = async (): Promise<RankedPlayer[]> => {
  const url = "https://api.sleeper.app/projections/nfl/2023";
  const params = [
    "season_type=regular",
    "position[]=DEF",
    "position[]=K",
    "position[]=QB",
    "position[]=RB",
    "position[]=TE",
    "position[]=WR",
    "order_by=adp_ppr",
  ].join("&");
  const res = await axios.get<PlayerRemote[]>(`${url}?${params}`);
  const { data } = res;
  const rankedPlayers = convertFromRemote(data);
  try {
    // attempt to add auction values if possible
    await addAuctionValuesToRankedPlayers(rankedPlayers);
  } catch (error) {
    // do nothing
  }
  return rankedPlayers;
};
