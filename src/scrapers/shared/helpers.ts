import puppeteer, { Browser, Page } from "puppeteer";

export const envVarToBoolean = (envVar: string | undefined) =>
  envVar && envVar.toLowerCase() === "true";

export const getIsInDebugMode = () => envVarToBoolean(process.env.DEBUG);

export const getPuppeteerOptions = () => {
  const options = {
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
    defaultViewport: {
      width: 1400,
      height: 900,
    },
  };

  if (getIsInDebugMode()) {
    return {
      ...options,
      headless: false,
    };
  }

  return options;
};

export const handlePageConsole = (page: Page) => {
  if (getIsInDebugMode()) {
    page.on("console", (msg) => console.log("PAGE LOG:", msg.text()));
  }
};

export const openBrowser = async () => {
  return await puppeteer.launch(getPuppeteerOptions());
};

export const closeBrowser = async (browser: Browser) => {
  return await browser.close();
};

export const openPage = async (browser: Browser) => {
  const page = await browser.newPage();
  handlePageConsole(page);
  return page;
};

export const closePage = async (page: Page) => {
  return await page.close();
};
