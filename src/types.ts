export enum Position {
  QB = "QB",
  RB = "RB",
  WR = "WR",
  TE = "TE",
  K = "K",
  DEF = "DEF",
}

export type Player = {
  id: string;
  name: string;
  team: string;
  position: Position;
};

export type RankedPlayer = {
  rank: number;
  customRank: number;
  overallRank: number | null;
  andyRank: number | null;
  mikeRank: number | null;
  jasonRank: number | null;
  upside: number | null;
  tier: number | null;
  bye: number | null;
  pointsStd: number | null;
  pointsPpr: number | null;
  pointsHalfPpr: number | null;
  risk: number | null;
  adpStd: number | null;
  adpPpr: number | null;
  adpHalfPpr: number | null;
  injury: string | null;
  injuryMessage: string | null;
  auctionValue: number | null;
} & Player;

export type OverallRankedPlayer = {
  overallRank: number | null;
  andyRank: number | null;
  mikeRank: number | null;
  jasonRank: number | null;
} & Player;

export type PlayersByPosition = {
  [key in Position]: Player[];
};

export type RankedPlayersByPosition = {
  [key in Position]: RankedPlayer[];
};

export enum ERROR {
  UNKNOWN = "UNKNOWN",
  MALFORMED_BODY = "MALFORMED_BODY",
  INVALID_USERNAME_OR_PASSWORD = "INVALID_USERNAME_OR_PASSWORD",
  INVALID_DRAFT_ID = "INVALID_DRAFT_ID",
  INVALID_UDK_AUTH_TOKEN = "INVALID_UDK_AUTH_TOKEN",
}

export type ErrorResponse = {
  error: ERROR;
  message: string;
};
