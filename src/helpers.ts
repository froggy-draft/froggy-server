import ms from "ms";
import _ from "lodash";
import { Response } from "express";
import { ErrorResponse, RankedPlayersByPosition, RankedPlayer } from "./types";

export const sendError = (
  res: Response,
  code: number,
  errorResponse: ErrorResponse
) => {
  res.status(code);
  res.send(errorResponse);
};

export const rejectAfterMs = (
  numberOfMs: number,
  errorMessage?: string
): Promise<void> => {
  return new Promise((_resolve, reject) => {
    setTimeout(() => {
      reject(
        new Error(errorMessage || `Timeout of ${ms(numberOfMs)} reached.`)
      );
    }, numberOfMs);
  });
};

export const withRejectTimeout = async (
  promise: Promise<any>,
  name: string,
  numberOfMs: number
) => {
  const errorMessage = `${name} failed to complete after ${ms(numberOfMs)}.`;
  await Promise.race([rejectAfterMs(numberOfMs, errorMessage), promise]);
};

export const makeWarmCacheFn = ({
  fn,
  cacheName,
  repeatAfterMs = ms("1h"),
  rejectAfterMs = ms("10m"),
}: {
  fn: (...x: any) => Promise<any>;
  cacheName: string;
  repeatAfterMs?: number;
  rejectAfterMs?: number;
}) => {
  let isCacheCold = true;

  return () => {
    const warmCache = async () => {
      if (isCacheCold) {
        console.log(`Warming "${cacheName}" cache...`);
      }
      try {
        await withRejectTimeout(
          fn(),
          `Warming of "${cacheName}" cache`,
          rejectAfterMs
        );
        if (isCacheCold) {
          console.log(`"${cacheName}" cache is now warm.`);
          isCacheCold = false;
        }
      } catch (error) {
        console.error(error);
      }
      setTimeout(warmCache, repeatAfterMs);
    };

    setTimeout(warmCache, 0);
  };
};

export const scrambleUdkPlayers = _.throttle(
  (playersByPosition: RankedPlayersByPosition): RankedPlayersByPosition => {
    const scramble = (players: RankedPlayer[]): RankedPlayer[] => {
      const ranks = _.shuffle(_.range(1, players.length + 1));

      return _.chain(players)
        .map((x, i) => ({ ...x, rank: ranks[i], customRank: ranks[i] }))
        .sortBy(["rank"])
        .map((x) => {
          const rank = x.rank;
          const risk = Number(_.random(0.5, 10.5).toFixed(1));
          const lowestRank = rank - 3 >= 1 ? rank - 3 : 1;
          const highestRank =
            rank + 3 <= players.length + 1 ? rank + 3 : players.length + 1;
          const andyRank = !x.andyRank
            ? x.andyRank
            : _.random(lowestRank, highestRank);
          const mikeRank = !x.mikeRank
            ? x.mikeRank
            : _.random(lowestRank, highestRank);
          const jasonRank = !x.jasonRank
            ? x.jasonRank
            : _.random(lowestRank, highestRank);
          return {
            ...x,
            risk,
            andyRank,
            mikeRank,
            jasonRank,
          };
        })
        .reduce<RankedPlayer[][]>(
          (acc, curr) => {
            const currentChunkIndex = acc.length - 1;
            const currentChunk = acc[currentChunkIndex];
            const thresholdOfNewChunk =
              currentChunk.length / 10 <= 1 ? currentChunk.length / 10 : 1;
            const thresholdResult = _.random(0, 1, true);
            if (thresholdResult <= thresholdOfNewChunk) {
              return [...acc, [curr]];
            }
            acc[currentChunkIndex] = [...currentChunk, curr];
            return acc;
          },
          [[]]
        )
        .map((x, i) => x.map((y) => ({ ...y, tier: i + 1 })))
        .flatten()
        .value();
    };

    return {
      QB: scramble(playersByPosition.QB),
      RB: scramble(playersByPosition.RB),
      WR: scramble(playersByPosition.WR),
      TE: scramble(playersByPosition.TE),
      DEF: scramble(playersByPosition.DEF),
      K: scramble(playersByPosition.K),
    };
  },
  10 * 1000
);
