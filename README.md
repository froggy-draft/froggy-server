# Froggy API Server

The API server to support the Froggy fantasy football drafting tool.

## Instructions for Digital Ocean Droplet

These are instructions for how to spin up the server on a Digital Ocean Ubuntu Droplet.

Sources:

- https://javascriptwebscrapingguy.com/setting-up-puppeteer-on-ubuntu-16-04-and-digital-ocean/
- https://gist.github.com/kevinpiac/782b86ff5ba272315d587becccc8a2ad
- https://github.com/puppeteer/puppeteer/issues/5661

### 1. Install system dependencies

```bash
sudo apt-get update

sudo apt-get install -y gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget libgbm-dev
```

### 2. Generate & install SSH Key

```bash
ssh-keygen -t rsa -b 4096

eval "$(ssh-agent -s)"

ssh-add ~/.ssh/id_rsa

cat ~/.ssh/id_rsa.pub
# Copy this public SSH key and add it to your Gitlab
```

### 3. Install nvm

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

You will need to either close and re-open a new session or copy/paste the env vars given to you into this session.

### 4. Install yarn

```bash
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt update && sudo apt install --no-install-recommends yarn
```

You will need to either close and re-open a new session or copy/paste the env vars given to you into this session.

### 5. Set up Certbot for HTTPS

Follow instructions at: https://certbot.eff.org/lets-encrypt/ubuntubionic-other.

You will probably need to run the following to add the hooks for auto-renewal:

```bash
sh -c 'printf "#!/bin/sh\nPATH=\"$PATH\"\ncd /root/froggy-server\n./scripts/pm2/stop\n" > /etc/letsencrypt/renewal-hooks/pre/froggy-server.sh'
sh -c 'printf "#!/bin/sh\nPATH=\"$PATH\"\ncd /root/froggy-server\n./scripts/pm2/start\n" > /etc/letsencrypt/renewal-hooks/post/froggy-server.sh'
chmod 755 /etc/letsencrypt/renewal-hooks/pre/froggy-server.sh
chmod 755 /etc/letsencrypt/renewal-hooks/post/froggy-server.sh
```

### 6. Clone & set up the repo

```bash
git clone git@gitlab.com:froggy-draft/froggy-server.git

cd froggy-server

nvm install && nvm use

yarn
```

### 7. Set up .env

```bash
cp .env.template .env

# Then, open .env and set up the environment variables
```

### 8. Start the server

```bash
./scripts/pm2/start
```

### Other Notes:

#### How to update packages on Digital Ocean Droplet

```bash
sudo apt-get update && sudo apt-get dist-upgrade
```

Source: <https://www.digitalocean.com/community/questions/how-to-update-packages-and-security-updates>
